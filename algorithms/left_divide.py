import numpy as np
from scipy.linalg import qr
from scipy import sparse
from scipy.sparse.linalg import spsolve


def left_divide(E, I, tol=1e-8):
    """
     Implements left division and overcomes many small inefficiencies
    :param E: The full rank system matrix
    :param I: The currents matrix (RHS)
    :param tol: The tolerance in the forward solution, e.g. 1e-5
    :return: old options from previous solver
    """
    return spsolve(E, I)
    # # [n_nodes, n_stims] = I.shape
    # # [R, Q] = np.linalg.qr(I.todense())
    # #
    #
    # # rnotzeros = (sA != 0.2)
    #
    # # import matplotlib.pylab as pltcd
    # # import scipy.sparse as sps
    # #
    # # plt.spy(sparse.csr_matrix(Q))
    # # plt.show()
    # # try:
    # '''
    #      V= E\I;
    #      This takes MUCH longer when you have  more vectors in I,
    #      even if they are repeated. There must be some way to simplify
    #      this to speed it up. Matlab's sparse operators really should
    #      do this for you.
    #      TODO, Q&R should be cached somewhere
    # '''
    #
    # # B = np.array([[2,0],[0,4]])
    # # b = np.array([[4,0],[0,4]])
    # #
    # # A = sparse.csr_matrix(B)
    # # a = sparse.csr_matrix(b)
    # x = spsolve(E,I)
    #
    # print()
    #
    # # except:
    # #     print("ERROR !!")
    #
    # V = 0
    # return V
