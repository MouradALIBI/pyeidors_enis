
from deprecated.bld_master_full import bld_master_full
from deprecated.ref_master import ref_master
import numpy as np

def fem_master_full(vtx, simp, mat, gnd_ind , elec, zc, perm_sym):
    """
    Builds up the system matrix based on the complete electrode model. E is not
    yet permuted. To permute E -> E(pp,pp) as in forward_solver.



    E       = The full rank system matrix based on the 3D complete electrode model
    D       = The sgradients of the shape functions over each element.
    Ela     = Normalised volums of the elements
    pp      = Column permutation vector, for more help type help symmmd
    vtx     = The vertices matrix
    simp    = The simplices matrix
    mat     = The conductivity vector
    gnd_ind = The index of the ground node
    elec    = The bounary electrodes matrix
    zc      = The contact impedance vector, satisfying size(elec,1) = length(zc)
    perm_sym= Column permutation of E, either '{y}' to opt or '{n}' to avoid.
    """

    [Ef, D, Ela] = bld_master_full(vtx, simp, mat, elec, zc)
    E = ref_master(Ef, vtx, gnd_ind-1)
    #import  scipy
    # pp = scipy.sparse.linalg.splu (E).perm_c ##  pp = colamd(M'*M)
    # pp = scipy.sparse.linalg.splu(E)
    # pp = scipy.sparse.linalg.dsolve.splu(E)
    #o = E[pp,pp]
    # import matplotlib.pyplot as plt
    # plt.plot(o)
    # plt.show()    # # Display matrix
    # import matplotlib.pyplot as plt
    # plt.spy(o)
    # plt.show()

    #
    # from scipy import sparse
    # b = sparse.csr_matrix(E)
    # data = b.data
    # jj = b.indices
    # ii = b.indptr.imag
    #
    # from cvxopt import spmatrix, amd , matrix
    #from scipy import sparse
    # sA =  scipy.sparse.csr_matrix(E)
    #
    # A = spmatrix([10, 3, 5, -2, 5, 2], [0, 2, 1, 2, 2, 3], [0, 0, 1, 1, 2, 3])
    # C = matrix(E)
    # print(type(C))
    # #A = spmatrix(sA)
    # P = amd.order((matrix(spmatrix(array(E)))))

    # pp = symmmd(E)
    # print(type(pp))
    # # Display matrix
    # import matplotlib.pyplot as plt
    # plt.spy(pp)
    # plt.show()

    E, D, Ela, pp = 0,0, 0, 0
    return E, D, Ela, pp
