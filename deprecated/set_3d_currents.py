# from numpy import *
from numpy import reshape
import numpy as np


def set_3d_currents(protocol, elec, vtx, gnd_ind, no_pl):
    """
    function [I,Ib]=set_3d_currents(protocol,elec,vtx,gnd_ind,no_pl);

    This function sets current patterns in a system with (no_pl) planes of
    equal number of electrodes according to "opposite" or "adjacent" protocols,
    or their 3D similar.



    protocol= The selected protocol '{op}' or '{ad}'
    elec    = The electrodes (only the number of electrodes is used)
    vtx     = The vertices
    gnd_ind = the index of the ground node
    no_pl   = The number of planes
    Ib      = The current patterns
    I       = The RHS vectors, i.e., the current patterns padded with zeroes
    """

    vr = int(len(vtx))
    vc = int(len(vtx[0]))
    el_no = int(len(elec))
    q = int(len(elec[0]))
    el_pp = int(el_no / no_pl)
    a = list(range(el_no))
    X = np.arange(el_no).reshape((no_pl, el_pp))
    if protocol == '{op}':
        Ib = []

    elif protocol == '{ad}':
        Ib = []

        # ====== ERROR Ip ' trnasposer '
        for i in range(no_pl):
            this_plane = X[i, :]

            for j in list(range(this_plane[0], this_plane[-1] + 1)):
                Ip = np.zeros(el_no)
                Ip[j] = 1
                if j + 1 <= this_plane[el_pp - 1]:
                    Ip[j+1] = -1
                else:
                    Ip[this_plane[0]] = -1  # the ring pattern
                Ib.append(Ip)
        Ib=np.transpose(Ib)
        Is_supl = []
        for ii in range(vr):
            Is_supl.append(np.zeros(len(Ib[0])))

        I=[]
        I = np.concatenate((Is_supl,Ib ), axis=0)
        I[gnd_ind-1,:] = 0
    else:
        print('protocol ',protocol,' is not recognized')






    return I, Ib
