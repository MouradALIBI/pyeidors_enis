from deprecated.np_fwd_parameters import np_fwd_parameters
from solvers.calc_system_mat import calc_system_mat


def np_fwd_solve(fwd_model):
    """
     NP_FWD_SOLVE: data= np_fwd_solve( fwd_model, img)
 Fwd solver for Nick Polydorides EIDORS3D code
 Input:
    fwd_model = forward model
    img       = image struct
 Output:
    data = measurements struct
 Options: (to return internal FEM information)
    img.fwd_solve.get_all_meas = 1 (data.volt = all FEM nodes, but not CEM)
    img.fwd_solve.get_all_nodes= 1 (data.volt = all nodes, including CEM)
    """

    img = fwd_model                         # normally takes one parameter      ''anyway''
    fwd_model = img.fwd_model

    p = np_fwd_parameters(fwd_model)
    # Set the tolerance for the forward solver
    tol = 1e-5
    s_mat = calc_system_mat(fwd_model, img)


    data = 0
    return data