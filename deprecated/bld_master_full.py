from deprecated.bld_master import bld_master
import numpy as np

from models.triarea3d import triarea3d


def bld_master_full_2d(vtx, simp, mat, elec, zc):
    pass


def bld_master_full_3d(vtx, simp, mat, elec, zc):
    vr = len(vtx)
    vc = len(vtx[0])
    sr = len(simp)
    sc = len(simp[0])
    er = len(elec)
    ec = len(elec[0])

    if len(mat) != sr:
        print("error('Invalid conductivity information for this mesh')")
    Ef, D, Ela = bld_master(vtx, simp, mat)
    Ef = np.lib.pad(Ef, ((0, vr + er - len(Ef[0])), (0, vr + er - len(Ef))), 'constant', constant_values=(0))
    # Up to this point we have calculated the master matrix without the influence of contact impedance.
    # The column vector zc with the contact
    # impedances in [Ohms] is required
    if len(zc) != er:
        print("error('zc (=%d) should be equal to er (=%d)', length(zc), er)")
    for q in range(er):
        tang_area = 0
        q_th_ele = (elec[q])[elec[q] != 0]  # Select the row of nodes corresponding to the current electrode
        if len(q_th_ele) == 1:  # check if point electrode
            m = q_th_ele
            cali_area = 1 / zc[q]
            tang_area = tang_area + cali_area
            Ef[m][vr + q] = Ef[m][vr + q] - cali_area / 2
            Ef[vr + q][m] = Ef[vr + q][m] - cali_area / 2
            Ef[m][m] = Ef[m][m] + cali_area / 2
        else:  # not point electrode - use complete electrode model
            for w in range(0, len(q_th_ele), 3):
                m = q_th_ele[w] - 1
                n = q_th_ele[w + 1] - 1
                l = q_th_ele[w + 2] - 1
                # This way m & n nodes belong to the edge tangential to the electrode
                # and also at the same simplex.
                # We will now evaluate the distance "tangential contact area" between m,n & l
                Are = triarea3d([vtx[m], vtx[n], vtx[l]])  # area mnl
                cali_area = np.divide(2 * Are, zc[q])  # coefficient for the area mnl :: |J_k| = 2*Are
                tang_area = tang_area + cali_area
                # Start modifying "expanding" the E master matrix
                Ef[m][vr + q] = Ef[m][vr + q] - cali_area / 6  # Kv -> Ec  -> Vertical bar
                Ef[n][vr + q] = Ef[n][vr + q] - cali_area / 6
                Ef[l][vr + q] = Ef[l][vr + q] - cali_area / 6
                Ef[vr + q][m] = Ef[vr + q][m] - cali_area / 6  # Kv' -> Ec' -> Horizontal bar
                Ef[vr + q][n] = Ef[vr + q][n] - cali_area / 6
                Ef[vr + q][l] = Ef[vr + q][l] - cali_area / 6

                Ef[m][m] = Ef[m][m] + cali_area / 12  # Kz -> E -> Main bar
                Ef[m][n] = Ef[m][n] + cali_area / 24
                Ef[m][l] = Ef[m][l] + cali_area / 24

                Ef[n][m] = Ef[n][m] + cali_area / 24
                Ef[n][n] = Ef[n][n] + cali_area / 12
                Ef[n][l] = Ef[n][l] + cali_area / 24

                Ef[l][m] = Ef[l][m] + cali_area / 24
                Ef[l][n] = Ef[l][n] + cali_area / 24
                Ef[l][l] = Ef[l][l] + cali_area / 12
            # dealing with this electrode
        # point electrode
        Ef[vr + q][vr + q] = Ef[vr + q][vr + q] + 0.5 * tang_area
    # for the whole set of electrodes

    return Ef, D, Ela


def bld_master_full(vtx, simp, mat, elec, zc):
    """
    function [Ef,D,Ela] = bld_master_full(vtx,simp,mat,elec,zc);
    System matrix assembling based on the complete electrode model.
    This function is called within fem_master_full
    :param Ef   = The UNreferenced system matrix.
    :param D    = The sgradients of the shape functions over each element.
    :param Ela  = Normalised volums of the elements
    :param vtx  = The vertices matrix. The coordinates of the nodes in 3D.
    :param simp = The simplices matrix. Unstructured tetrahedral.
    :param mat  = As for MATerial information. The conductivity vector.(isotropic)
    :parame lec = The matrix that holds the boundary faces assigned as electrodes. Its typical
           dimensions are [number of electrodes x 3*number of faces per electrode].
    :param zc   = The array of electrode contact impedances.
    """

    dimen = len(vtx[0])
    if dimen == 2:
        [Ef, D, Ela] = bld_master_full_2d(vtx, simp, mat, elec, zc)
    elif dimen == 3:
        [Ef, D, Ela] = bld_master_full_3d(vtx, simp, mat, elec, zc)
    else:
        print("error('not 2d or 3d')")
    return Ef, D, Ela
