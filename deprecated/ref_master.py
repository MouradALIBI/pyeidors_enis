import numpy as np
def ref_master(E, vtx, gnd_ind, sch=0):  # sch = 0 Ground a surface node
    """
    function [Er] = ref_master(E,vtx,gnd_ind,sch);

     Applying reference to the system. Modifying the system matrix to
     preserve uniqueness.



     E       = The rank deficient by 1 system matrix
     Er      = The full rank matrix
     sch     = The grounding scheme:
               0 for grounding node gnd_ind
               1 for grounding electrode gnd_ind
     gnd_ind = The ground index
    """
    nv = len(vtx)
    jnk = len(vtx[0])
    Er = E
    Er[gnd_ind] = 0  # zeros(1, mas_c);
    Er[:,gnd_ind]=0
    Er[gnd_ind,gnd_ind] = 1
    return Er
