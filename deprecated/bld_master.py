import numpy as np
import sparse


def bld_master_2d(vtx, simp, mat_ref):
    pass


# 3D BLD MASTER
def bld_master_3d(vtx, simp, mat_ref):
    nv = len(vtx)  # Number of vertices and dimension
    dimen = len(vtx[0])  # Number of vertices and dimension
    ns = len(simp)  # Number of simplices
    dimen_p1 = len(simp[0])  # Number of simplices
    a = mat_ref
    dimen2 = 2 * dimen
    ils = range(dimen * ns)
    ilst = []
    for i in ils:
        ilst.append(i)
        ilst.append(i)
        jlst = np.zeros(ns * dimen2)
    patt = range(0, ns * dimen2, dimen2)
    simp = simp - 1
    for i in range(len(simp)):
        x = patt[i]
        jlst[x] = simp[i][0]
        jlst[x + 1] = simp[i][1]
        jlst[x + 2] = simp[i][0]
        jlst[x + 3] = simp[i][2]
        jlst[x + 4] = simp[i][0]
        jlst[x + 5] = simp[i][3]
    sls = np.ones(dimen * ns)
    slst = []
    for i in range(dimen * ns):
        slst.append(-1)
        slst.append(1)
    D0 = sparse.COO((ilst, jlst), slst, shape=(dimen * ns, nv))
    D0 = D0.todense()  # D0 is the matrix of the definitions of the gradients on elements
    vtx = np.transpose(vtx)
    J1 = D0.dot(vtx[0])
    J2 = D0.dot(vtx[1])
    J3 = D0.dot(vtx[2])
    JJ = np.zeros(shape=(3, 3, ns))
    for w in range(3):
        rag = range(w, ns * dimen, dimen)
        JJ[0][w] = J1[rag]
        JJ[1][w] = J2[rag]
        JJ[2][w] = J3[rag]
    dj = np.squeeze(
        np.sum([np.prod([JJ[0][0], JJ[1][1], JJ[2][2]], axis=0), np.prod([JJ[0][1], JJ[1][2], JJ[2][0]], axis=0),
                np.prod([JJ[0][2], JJ[1][0], JJ[2][1]], axis=0), np.prod([-JJ[0][2], JJ[1][1], JJ[2][0]], axis=0),
                np.prod([-JJ[0][0], JJ[1][2], JJ[2][1]], axis=0), np.prod([-JJ[0][1], JJ[1][0], JJ[2][2]], axis=0)],
               axis=0))
    ilst = np.kron((range(dimen * ns)), np.ones(dimen))
    jlst = np.zeros(ns * (dimen ** 2))
    for d in range(dimen):
        jlst[range(d, ns * dimen ** 2, dimen)] = np.kron(range(d, dimen * ns, dimen), np.ones(dimen))
    slst = np.zeros(ns * dimen ** 2)  #
    pat = np.array(range(0, ns * dimen ** 2, dimen ** 2))
    slst[pat] = np.sum([np.prod([JJ[1][1], JJ[2][2]], axis=0), np.prod([-JJ[1][2], JJ[2][1]], axis=0)], axis=0) / dj
    slst[pat + 1] = np.sum([np.prod([JJ[2][0], JJ[1][2]], axis=0), np.prod([-JJ[1][0], JJ[2][2]], axis=0)], axis=0) / dj
    slst[pat + 2] = np.sum([np.prod([JJ[1][0], JJ[2][1]], axis=0), np.prod([-JJ[2][0], JJ[1][1]], axis=0)], axis=0) / dj
    slst[pat + 3] = np.sum([np.prod([JJ[2][1], JJ[0][2]], axis=0), np.prod([-JJ[0][1], JJ[2][2]], axis=0)], axis=0) / dj
    slst[pat + 4] = np.sum([np.prod([JJ[0][0], JJ[2][2]], axis=0), np.prod([-JJ[0][2], JJ[2][0]], axis=0)], axis=0) / dj
    slst[pat + 5] = np.sum([np.prod([JJ[2][0], JJ[0][1]], axis=0), np.prod([-JJ[0][0], JJ[2][1]], axis=0)], axis=0) / dj
    slst[pat + 6] = np.sum([np.prod([JJ[0][1], JJ[1][2]], axis=0), np.prod([-JJ[1][1], JJ[0][2]], axis=0)], axis=0) / dj
    slst[pat + 7] = np.sum([np.prod([JJ[1][0], JJ[0][2]], axis=0), np.prod([-JJ[0][0], JJ[1][2]], axis=0)], axis=0) / dj
    slst[pat + 8] = np.sum([np.prod([JJ[0][0], JJ[1][1]], axis=0), np.prod([-JJ[1][0], JJ[0][1]], axis=0)], axis=0) / dj
    LocJac = sparse.COO((ilst, jlst), slst, shape=(dimen * ns, dimen * ns))
    LocJac = LocJac.todense()
    D = np.dot(LocJac, D0)
    Vols = np.absolute(dj) / 6
    materials = len(a)
    volumes = len(Vols)
    if materials != volumes:
        print("error('Some elements have not been assigned')")
    # This is for the global conductance matrix (includes conductivity)
    Ela = (sparse.COO((range(dimen * ns), range(dimen * ns)), np.kron(np.transpose(a * Vols), np.ones(dimen)))).todense()
    Ef = np.dot(np.dot(np.transpose(D),Ela),D)
    # This is for the Jacobian matrix (does not include conductivity)
    Ela = (sparse.COO((range(dimen * ns), range(dimen * ns)), np.kron(np.transpose(Vols), np.ones(dimen)))).todense()
    return Ef, D, Ela


def bld_master(vtx, simp, mat_ref):
    """
    function [Ef,D,Ela] = bld_master(vtx,simp,mat_ref);
    Builds up the main compartment (GAP-SHUNT) of the system matrix
    for the complete electrode model. It is called within the function
    fem_master_full.
    :param Ef      = The UNreferenced GAP based system matrix
    :param D       = The sgradients of the shape functions
    :param           over each element.
    :param Ela     = Normalised volums of the elements
    :param vtx     = The vertices matrix.
    :param simp    = The simplices matrix.
    :param mat_ref = The reference CONDUCTIVITY at each element.
    :param In the complex case mat_ref(i) = sigma(i) - epsilon(i)
    """
    dimen = len(vtx[0])
    if dimen == 2:
        [Ef, D, Ela] = bld_master_2d(vtx, simp, mat_ref);
    elif dimen == 3:
        [Ef, D, Ela] = bld_master_3d(vtx, simp, mat_ref);

    else:
        print("error('not 2d or 3d')")
    return Ef, D, Ela
