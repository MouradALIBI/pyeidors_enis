import numpy as np


def triarea3d(V):
    """
    [ta] = triarea3d[V]][
     
     Function that calculates the area of a triangle
     in the 3D Cartesian space. 
     V = the 3 x 3 coordinates matrix of the points][ first
     column for the xs and last for the zs.
     ta = the area of the triangle
    """
    p1 = np.array([[V[0][1], V[0][2], 1], [V[1][1], V[1][2], 1], [V[2][1], V[2][2], 1]])
    p2 = np.array([[V[0][2], V[0][0], 1], [V[1][2], V[1][0], 1], [V[2][2], V[2][0], 1]])
    p3 = np.array([[V[0][0], V[0][1], 1], [V[1][0], V[1][1], 1], [V[2][0], V[2][1], 1]])
    ta = 0.5 * np.sqrt((np.linalg.det(p1)) ** 2 + (np.linalg.det(p2)) ** 2 + (np.linalg.det(p3)) ** 2)
    return ta
