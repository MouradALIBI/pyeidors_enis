#EIDORS_OBJ: 'constructor' to create a eidors structure
#   example: fwd_mdl = ...
#       eidors_obj('fwd_model','My FWD MODEL', ...
#                   'nodes', NODES, 'elems', ELEMS, ...
import scipy.io


from scipy import *

def eidors_var_id():

    return "id_C7BECB83EAAC95AC8D2CDA859B45CC0D107DAF78 "


def calc_obj_id(var):
    obj_id= 0

    try:
        obj_id = eidors_var_id()
    except:
        print("ERROR : id_C7BECB83EAAC95AC8D2CDA859B45CC0D107DAF78 NOT EXIST  ")

    return  obj_id



def get_cache_obj(obj, prop ):
    varargin = []
    obj_id = calc_obj_id(obj)  # recalculate in case obj changed

    mat = scipy.io.loadmat('../matlab.mat')
    vtx = mat['def']



    # eidors_objects = scipy.io.loadmat('../matlab.mat')
    # value = eidors_objects['cache_path']
    # print(value)
    # value= eidors_objects.( obj_id ).cache.( prop );
    # value= eval(sprintf('eidors_objects.%s.cache.%s;',obj_id,prop));

    value = []
    return value



def eidors_obj(type,name, varargin ):

    if(type == 'get-cache'):
        obj_id = []
        obj_id = get_cache_obj(name, varargin )

    obj_id=0
    return obj_id