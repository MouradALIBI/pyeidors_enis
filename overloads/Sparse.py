import sparse
import numpy as np

def Sparse(x,y,z,a,b):
    """
    SPARSE Create sparse matrix (PYEIDORS overload).
       S = SPARSE(X) converts a sparse or full matrix to sparse form by
       squeezing out any zero elements.

      See also SPARFUN/SPARSE

     Sparse doesn't work for double * inputs, so we need to preconvert to int


    # """

    # D0 = np.zeros(shape=(a ,b))
    # for i in range(len(x)):
    #     x = int(x[i])
    #     y = int(y[i])
    #     z = int(z[i])
    #     D0[x][y] = z
    # x=np.array(x,dtype=int)
    # y=np.array(y,dtype=int)
    # z=np.array(z,dtype=int)
    # x  = x.astype(int)
    # y = y.astype(int)
    # z = z.astype(int)
    # x = int(x)
    # y = int(y)
    # z = int(z)
    S = sparse.COO((x, y), z,a,b)
    S = S.todense()


    return S