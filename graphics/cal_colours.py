import scipy.io

from graphics.scale_for_display import scale_for_display

eidors_colours = scipy.io.loadmat('../graphics/eidors_colours.mat')['eidors_colours']
from solvers.get_img_data import get_img_data


def get_field(param):
    # eidors_colours = scipy.io.loadmat('../graphics/eidors_colours.mat')['eidors_colours']
    if (param == 'colourmap'):
        ncol = eidors_colours['mapped_colour']
        # [red, grn, blu] = blu_red_axis(eidors_colours,  [-1, linspace(-1, 1, 2 * ncol - 1)]', backgndidx )
        # value = [red, grn, blu]
        pass
    else:
        value = (eidors_colours[param]).item().item()
    return value


def get_colours(img):
    pp = eidors_colours
    # pp.add('component' ,'real') pp.component = 'real'  # Don't get from globals
    return pp


def calc_colours(img, set_value=0, do_colourbar=0):
    # [colours,scl_data]= calc_colours(img, set_value, do_colourbar)
    # Calculate a colour for each image element
    #
    # Conductive (positive) areas are shown in red
    # Non-Conductive (negative) areas are shown in blue
    #
    # PARAMETERS: img
    #     - 1) an PyEIDORS image object, or a Ex1 vector
    #     - 2) a 2D image matrix
    #
    # Usage #1 (img is a Ex1 vector of element conductivities)
    #
    #   Cs = calc_colours( img);
    #   patch(Xs,Ys,Zs,Cs);
    #
    # Cs is Ex1x1 colourmap entries (if mapped_colour>0)
    #       Ex1x3 colourmap entries (if mapped_colour==0)
    #
    # Usage #2 (rimg is a MxN image matrix of reconstructed pixels):
    #           img is an image structure with the image properties
    #
    #   c_img = calc_colours( rimg, img);
    #   image( c_img );
    #
    # c_img is MxN colourmap entries (if mapped_colour>0)
    #          MxNx3 colourmap entries (if mapped_colour==0)
    #
    # Usage #3 (img is string parameter value)
    #
    #   value = calc_colours( 'param' );
    #   calc_colours( 'param', value );
    #    eg. calc_colours('mapped_colour',127)
    #         Use this to allow printing vector eps files to get around
    #         a matlab bug with warning 'RGB color data not ...'
    #
    #   calc_colours( 'component', 'real' ); (DEFAULT real)
    #     the other value is 'imag' to show the imaginary component
    #
    #   The following parameters are accepted
    #
    #   'greylev'    (DEFAULT -.01): the colour of the ref_level.
    #      Negative values indicate white background
    #      For almost white, greylev=-.01; Black=> greylev=.01
    #   'sat_adj'    (DEFAULT .9): max G,B when R=1
    #   'window_range' (DEFAULT .9); window colour range
    #      Colour slope outside range is 1/3 of centre slope
    #   'backgnd' ( DEFAULT [.5,.5,.15] ): image border colour
    #   'ref_level' (DEFAULT 'auto') conductivity of centre of
    #      colour mapping. 'auto' tries to estimate a good level.
    #      For complex image data, ref_level should also be complex.
    #   'mapped_colour' (DEFAULT 127) number of colourmap entries
    #      using mapped_colour allows matlab to print vector graphics to eps
    #      setting mapped_colour=0 means to use RGB colours
    #   'npoints' (DEFAULT 64) number of points accross the image
    #   'transparency_thresh' fraction of maximum value at which colours
    #             are rendered transparent for
    #   'clim'    (DEFAULT []) crop colour display of values above clim
    #           colour limit. values more different from ref_level are cropped.
    #           if not specified or clim==[] => no limit
    #   'cmap_type'  Specify special colours (Default 'blue_red')
    #           'blue_red':          default Blue/Red PyEIDORS colourmpa
    #           'jet':               matlab jet colourmap
    #           'jetair':            scaled jet colours
    #           'blue_yellow':       Blue/Yellow colours
    #           'greyscale':         Greyscale colours (Lungs white)
    #           'greyscale-inverse': Greyscale colours (Lungs black)
    #           'copper':            Copper colours
    #           'blue_white_red':    Blue/White/Red colours
    #           'black_red':         Black/Red Colours
    #           'blue_black_red':    Blue/Black/Red colours
    #           'polar_colours':     "Polar" blue/white/red colours
    #           'draeger', 'draeger-difference':
    #                                Draegerwerk colourmap (difference)
    #           'draeger-2009':      Draegerwerk colourmap (2009)
    #           'draeger-tidal':     Draegerwerk colourmap (tidal images)
    #           'swisstom'           Swisstom colourmap
    #           'timpel'             Timpel colourmap
    #           matrix [Nx3]         Use values ([0-1]) as REB colourmap
    #   'cb_shrink_move' shrink or move the colorbar. See PyEIDORS_colourbar
    #           help for details.
    #   'image_field', 'image_field_idx', 'image_field_val'
    #           image elements which match _idx are set to _val.
    #   'defaults' set to default colours in list above
    #
    #   'colourmap' Return the current PyEIDORS colormap.
    #           Use as colormap(calc_colours('colourmap'))
    #
    # PARAMETERS CAN BE SPECIFIED IN TWO WAYS
    #   1. as an image parameter (ie clim in img.calc_colours.clim)
    #   2. a second parameter to ( calc_colours(data, param2 )
    #          where param2.calc_colours.clim= ... etc
    #   3. parameter to calc_colours('clim')
    #
    # Parameters specified as (1) will override (2)
    #
    # PARAMETERS: do_colourbar
    #    - show a Matlab colorbar with appropriate scaling
    #
    #  usage: c_img= calc_colours( img, clim );
    #         image( c_img );
    #         calc_colours( img, clim, 1); # now do colorbar
    #
    # PARAMETERS: ref_lev
    #     - if specified, override the global ref_level parameter

    # if (type(img)== 'str':
    #     called as calc_colours('parameter' ... )
    # if (img =='defaults'):
    # set defaults and return
    # set_colours_defaults
    if (isinstance(img, str)):
        colours = get_field(img)
        return colours  # ,scl_data
    else:
        img_data = get_img_data(img)
        pp = get_colours(img)
    # Set default parameters
    do_colourbar = 0
    ref_lev = 'use_global'
    if (img_data == []):
        colours = 'k' # black
        return colours
    m = len(img_data)
    n = len(img_data[0])
    # We can only plot the real part of data
    # Vectorize img_data here, it gets reshaped later
    [scl_data, ref_lev, max_scale] = scale_for_display(img_data, pp,eidors_colours)

    colours = 0
    return colours