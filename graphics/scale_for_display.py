import  numpy as np
from math import floor
def scale_for_display(elem_data, pp, eidors_colours):
    """
 [elem_data,ref_lev,max_scale] = scale_for_display( elem_data, pp)

 PARAMETERS: elem_data
  elem_data: data for fem elements or image pixels

 PARAMETERS: ref_lev
  ref_lev:   if param provided, use it,
               otherwise use the global value
             Can be numeric or 'auto' or 'use_global'

 PARAMETERS: clim
    clim - colour limit. Colours more different from ref_level are cropped.
         - if not specified or scale==[] => no limit

 OUTPUT:
    ref_lev, max_scale - the centre and max of the colour scale
    elem_data - data scaled in the range [-1 .. 1]

    """
    clim = []
    if isinstance(pp, str) and pp == 'use_global':
        ref_lev = eidors_colours.ref_level
        component = 'real'
    else:
        ref_lev = pp['ref_level'].item().item()
        clim = pp['clim']
        component = 'real'  # pp['component']
        print(not isinstance(ref_lev,int))
    if not isinstance(ref_lev, int):
        if (ref_lev != 'auto'):
            print('error(ref_level parameter must be "auto" or numeric)')
        s_ed = elem_data[0][0]
        s_ed = np.sort(s_ed)
        e = len(s_ed)
        if e == 0:
            print("error(Can't display. All values NaN. Is raw data 0?)")
        # ensure symmetric rejection of data for small data sets
        #   This means that 1-2*.35 = .3 of the data will be used
        #   to take the mean. It should make for better centring
        #   of reconstructions
        sss = .35
        rej_vals = floor(.35 * e)

        pass
    elem_data, ref_lev, max_scale = 00, 0, 0
    return elem_data, ref_lev, max_scale
