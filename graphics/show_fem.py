import numpy as np

from graphics.cal_colours import calc_colours


class Opts:
    def __init__(self):
        pass

    def __init_subclass__(self, do_colourbar, number_electrodes, show_numbering, show_elem_numbering,
                          show_node_numbering, transparency_thresh, dims):
        self.do_colourbar = do_colourbar
        self.number_electrodes = number_electrodes
        self.show_numbering = show_numbering
        self.show_elem_numbering = show_elem_numbering
        self.show_node_numbering = show_node_numbering
        self.transparency_thresh = transparency_thresh
        self.dims = dims


def proc_params(mdl, options):
    opts = Opts()
    opts.do_colourbar = 0
    opts.number_electrodes = 0
    opts.show_numbering = 0
    opts.show_elem_numbering = 0
    opts.show_node_numbering = 0
    # fill in default options
    optionstr = np.zeros(100)
    optionstr[0: len(options)] = options
    opts.do_colourbar = optionstr[0]
    opts.number_electrodes = optionstr[1]
    if (optionstr[2] == 1):
        opts.show_elem_numbering = 1
    elif (optionstr[2] == 2):
        opts.show_elem_numbering = 2
    elif (optionstr[2] == 3):
        opts.show_elem_numbering = 3
    # if we have an only img input, then define mdl
    if mdl.type == 'image':
        img = mdl
        mdl = img.fwd_model
    else:
        img = []
    opts.transparency_thresh = calc_colours('transparency_thresh')
    try:
        opts.transparency_thresh = img.calc_colours.transparency_thresh
    except:
        pass
    opts.dims = mdl.mdl.nodes.shape[1]
    return [img, mdl, opts]


# 3D Case
def show_3d(img, mdl, opts):
    pass


# 2D Case
def show_2d(img, mdl, opts):
    if (img):
        colours = calc_colours(img, [])
    else:
        colours = [1, 1, 1]  # white elements if no image

    hh = 0
    return hh


def show_fem(mdl, options=[]):
    """
 SHOW_FEM: show the PYEIDORS3D finite element model
 hh=show_fem( mdl, options )
 mdl is a PYEIDORS3D 'model' or 'image' structure
 hh= handle to the plotted model

 options may be specified by a list

 options specifies a set of options
   options(1) => show colourbar
   options(2) => show numbering on electrodes
   options(3) => number elements (==1) or nodes (==2);

 for detailed control of colours, use
    img.calc_colours."parameter" = value
 see help for calc_colours.

 for control of colourbar, use img.calc_colours.cb_shrink_move

 parameters
     fwd_model.show_fem.linecolour

 to change line properties:
      hh=show_fem(...); set(hh,'EdgeColor',[0,0,1];
    """

    [img, mdl, opts] = proc_params(mdl, options)
    if opts.dims == 2:
        show_2d(img, mdl, opts)
    elif opts.dims == 2:
        show_3d(img, mdl, opts)
    else:
        print('error(model is not 2D or 3D)')

    hh = 0
    return hh
