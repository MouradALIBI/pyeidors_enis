import numpy as np


class info:
    def __init__(self, step_size, error):
        self.step_size = step_size
        self.error = error
class Img_bkgnd:
    def __init__(self, type, name, elem_data, fwd_model,info):
        self.type = type
        self.name = name
        self.elem_data = elem_data
        self.fwd_model = fwd_model
        self.info = info

def feval(func, inv_model):
    pass


def has_params(jacobian_bkgnd):
    pass


def jacobian_bkgnd(inv_model):
    if inv_model.jacobian_bkgnd == 'func':
        img_bkgnd = feval(inv_model.jacobian_bkgnd.func, inv_model)
    elif inv_model.jacobian_bkgnd.value:
        if not inv_model.jacobian_bkgnd:
            print("warning('Ignoring parametrization-specific fields in Jacobian background')")
            # allow bkgnd to be scalar or vector
        fwd_model = inv_model.fwd_model
        bkgnd = np.ones(len(fwd_model.mdl.elems))
        bkgnd[:] = inv_model.jacobian_bkgnd.value
        img_bkgnd = Img_bkgnd('image', 'background image', bkgnd, fwd_model,info)
    else:
        img_bkgnd = 0
    return img_bkgnd


def calc_jacobian_bkgnd(inv_model):
    """
 CALC_JACOBIAN_BKGND: calculate background image around
    which initial estimate of jacobian is calculated
 
 img_bkgnd = calc_jacobian_bkgnd( inv_model )
 inv_model   is an EIDORS fwd_model 
 img_bkgnd   is an EIDORS struct

 Usage: 
      The background for calc_jacobian may be specified
      as an estimated value
  inv_model.jacobian_bkgnd.value;   scalar OR
                                    vector of Nx1

 Usage:
      The background may be calculated by a function
  inv_model.jacobian_bkgnd.func;
    """
    img_bkgnd = jacobian_bkgnd(inv_model)
    return img_bkgnd
