from solvers.calc_difference_data import calc_difference_data
from solvers.calc_jacobian import calc_jacobian
from solvers.calc_jacobian_bkgnd import calc_jacobian_bkgnd


def get_RM(inv_model):
    img_bkgnd = calc_jacobian_bkgnd(inv_model)
    J = calc_jacobian(img_bkgnd)
    #
    # RtR = calc_RtR_prior(inv_model);
    # W = calc_meas_icov(inv_model);
    # hp = calc_hyperparameter(inv_model);
    #
    # RM = (J'*W*J +  hp^2*RtR)\J' * W;
    RM = 0
    return RM


def inv_solve_diff_GN_one_step(inv_model, data1, data2):
    """
 INV_SOLVE_DIFF_GN_ONE_STEP inverse solver using approach of Adler&Guardo 1996
 img= inv_solve_diff_GN_one_step( inv_model, data1, data2)
 img        => output image (or vector of images)
 inv_model  => inverse model struct
 data1      => differential data at earlier time
 data2      => differential data at later time

 both data1 and data2 may be matrices (MxT) each of
  M measurements at T times
 if either data1 or data2 is a vector, then it is expanded
  to be the same size matrix

 By default, the correct scaling of the solution that best fits the data
 is not calculated, possibly resulting in high solution errors reported by
 inv_solve. 
 To calculate the correct scaling, specify
     inv_model.inv_solve_diff_GN_one_step.calc_step_size = 1;
 To provide a pre-calculated scaling, specify
     inv_model.inv_solve_diff_GN_one_step.calc_step_size = 0;
     inv_model.inv_solve_diff_GN_one_step.step_size = 0.8;
 The search for correct step_size is performed using FMINBND. The default
 search interval is [1e-5 1e1]. You can modify it by specifying:
     inv_model.inv_solve_diff_GN_one_step.bounds = [10 200];
 Additional options for FMINBD can be passed as:
     inv_model.inv_solve_diff_GN_one_step.fminbnd.MaxIter = 10;

 The optimal step_size is returned in img.info.step_size.

 See also INV_SOLVE, CALC_SOLUTION_ERROR, FMINBND
 TODO:
 Test whether Wiener filter form or Tikhonov form are faster
 Tikhonov: RM= (J'*W*J +  hp^2*RtR)\J'*W;
 Wiener:   P= inv(RtR); V = inv(W); RM = P*J'/(J*P*J' + hp^2*V)
    """
    dv = calc_difference_data(data1, data2, inv_model.fwd_model)
    ##sol = eidors_cache( @ get_RM, inv_model, 'inv_solve_diff_GN_one_step' ) *dv;
    sol = get_RM(inv_model)
    sol = dv ## dv = 0 ! don't neet to build get_RM(inv_model)
    #img = data_mapper(calc_jacobian_bkgnd(inv_model))
    img = (calc_jacobian_bkgnd(inv_model))
    img.current_params = 'conductivity'
    img.name = 'solved by inv_solve_diff_GN_one_step'
    img.elem_data = sol
    img.fwd_model = inv_model.fwd_model
    img.info.step_size = 1
    img.info.step_size  = 0
    return img
