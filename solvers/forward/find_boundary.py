"""
Calculates the boundary faces of a given 3D volume.
Useful in electrode assignment.
"""

# srf  =  array of elements on each boundary simplex
#         srf  =  array of elements on each boundary simplex
# idx  =  index of simplex to which each boundary belongs
# simp = The simplices matrix
# import sys
# sys.path.append('../../')

# import message as msg


########################################
##  sort surfaces. If there  is more  ##
##  than one, its   not on the boundary ##
########################################
from solvers.forward.nchoosek import nchoosek
import numpy as np
from numpy import *
import numpy as np


########################################
##  permiataion matrix par matrix    ###
########################################

def permitation_matrx_patrix(A, B):
    BB = []
    D = []
    DD = []
    for index in range(len(A)):
        A[index] = np.array(A[index]).tolist()
    for index in range(len(B)):
        B[index] = np.array(B[index]).tolist()
    A = np.array(A).tolist()
    for i in B:
        for j in range(len(i)):
            BB.append(i[j])
    for i in range(len(A)):
        D = []
        for j in range(len(BB)):
            x = int(BB[j])
            D.append(A[i][x - 1])
        DD.append(D)
    return DD


################################
##  function reshape
################################


def find_2or3d_boundary(simp, dim):
    # convert "simp"to integer to make sort faster !!!
    localface = []
    localface = list(nchoosek(range(dim + 1), dim))

    sfr_local = simp[:, localface]
    ################################################################################################################
    ################################################################################################################
    sfr_localc = []
    for el in sfr_local:
        x = []
        for e in el:
            for ee in e:
                x.append(ee)
        sfr_localc.append(x)
    sfr_local = sfr_localc
    l = len(sfr_local)
    c = len(sfr_local[1])
    sfr_local = reshape(np.transpose(sfr_local), (dim, int(l * c / dim)))
    ################################################################################################################
    ################################################################################################################

    argsort(sfr_local, axis=0)
    sfr_local = np.transpose(sfr_local)
    sort_srl = np.array(sfr_local)
    sort_srl.argsort(axis=1)
    sort_srl = sort(sort_srl, axis=1)
    sort_srl = sort(sort_srl, axis=0)
    sort_idx = list(range(len(sfr_local)))


    # sort_srl = sort(np.transpose(sfr_local))

    #########################################
    ##  Fine the ones that are the same    ##
    #########################################

    first_ones = sort_srl[:-1, :]
    next_ones = sort_srl[1:, :]

    same_srl = (all(first_ones == next_ones, 1))
    same_srl = same_srl[nonzero(same_srl)]

    ############################################################
    ##Assume they're all different. then find the same ones   ##
    ############################################################
    diff_srl = np.ones(len(sort_srl[0]), int)

    # diff_srl = (diff_srl >= 1)
    diff_srl[same_srl] = 0
    diff_srl[same_srl + 1] = 0
    sort_srl = np.transpose(sort_srl)
    srf = sort_srl[diff_srl, :]
    idx = sort_idx[:]
    myInt = dim + 1

    idx[:] = [ceil((x + 1) / myInt) for x in idx]

    # idx = ceil(int(idx / (dim + 1)))

    # x , y = a[a[:, 0].argsort(),]
    # [sort_srl, sort_idx] = np.msort(sfr_local,axis=0)

    # sfr_local =np.sort(sfr_local, axis=1)
    # sfr_local = np.transpose(np.sort(sfr_local))
    # '[sort_srl, sort_idx] = np.sortrows(sfr_local)

    # sfr_local = reshape (sfr_local, (dim-1,(int(l*c/dim)))

    # print(len(r))

    # print(list(localface))
    # srf_local = simp(:,localface)
    # print(simp.size)
    #####localface = list(nchoosek(range(1, dim + 2), dim));
    ########srf_local= = = = = = == = = = line 37 into find_boundary . m

    return srf

    ####################################
    ##  find_boundary => a ] 3D or 2

    # D ##
    ##            ##    => b ] 1D       ##
    ##  ELSE          => ERROR        ##
    ####################################


def find_boundary(simp):
    wew = simp[2].size - 1
    # print(" wew = ",wew)
    if wew == 3 or wew == 2:
        srf = find_2or3d_boundary(simp, wew)
    elif wew == 1:
        # srf, idx = find_1d_boundary(simp)
        bdy = 1
    else:
        print('find_boundary: WARNING: not 1D, 2D or 3D simplices')
        exit()

    bdy = srf
    return bdy
