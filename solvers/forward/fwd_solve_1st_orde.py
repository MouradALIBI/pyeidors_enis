import scipy
import numpy as np
from scipy import sparse

from algorithms.left_divide import left_divide
from solvers.calc_system_mat import calc_system_mat
from solvers.forward.fwd_model_parameters import fwd_model_parameters
def with_coo(x,y,a,b):
    x=x.tocoo()
    y=y.tocoo()
    d = np.concatenate((x.data, y.data))
    r = np.concatenate((x.row, y.row + a))
    c = np.concatenate((x.col, y.col + b))
    C = sparse.coo_matrix((d,(r,c)))
    return C


def get_v2meas(n_elec, n_stim, stim):
    # v2meas = sparse.csr_matrix(np.zeros(n_elec * n_stim))
    v2meas =np.transpose(stim[0]['meas_pattern'])
    for i in range(1, n_stim):
        meas_pat = stim[i]['meas_pattern']
        n_meas = meas_pat.shape[0]
        v2meas = with_coo(v2meas,np.transpose(meas_pat), n_elec * i, n_meas * i)
    return v2meas


def meas_from_v_els(v_els, stim):
    """
 Was 1.82s
         measured voltages from v
       vv = zeros( pp.n_meas, 1 );
        idx=0;
        for i=1:length(stim)
           meas_pat= stim(i).meas_pattern;
           n_meas  = size(meas_pat,1);
           vv( idx+(1:n_meas) ) = meas_pat*v_els(:,i);
           idx= idx+ n_meas;
        end
 This code replaced the previous - Nov 4, 2013
 Now 0.437s
 Why is it faster??
    """
    [n_elec, n_stim] = v_els.shape
    v2meas = get_v2meas(n_elec, n_stim, stim)
    v_els = v_els.toarray().flatten()
    vv = np.transpose(v2meas) * v_els[0:255]
    return vv


def fwd_solve_1st_orde(fwd_model, img, data):
    """
    FWD_SOLVE_1ST_ORDER: data= fwd_solve_1st_order( img)
    Fwd solver for Andy Adler's EIT code
    :param fwd_model:
    :param img: = image struct
    :return: data = measurements struct
    """
    img.current_params = "conductivity"
    pp = fwd_model_parameters(fwd_model)

    try:
        s_mat = calc_system_mat(img)
    except:
        s_mat = scipy.io.loadmat('system_mat.mat')['system_mat']

    idx = list(range(len(s_mat['E'][0][0].todense())))
    del idx[int(fwd_model.gnd_node) - 1]

    a = s_mat['E'][0][0]
    b = pp.QQ
    v = np.zeros(shape=(pp.n_node, pp.n_stim))
    v = left_divide(a, b)
    #  calc voltage on electrodes

    # This is horribly inefficient, override
    # v_els= pp.N2E * v
    idx = np.nonzero(np.any(pp.N2E))[1]
    v_els = pp.N2E[:, idx] * v[idx, :]

    # create a data structure to return
    data.meas = meas_from_v_els(v_els, fwd_model.stimulation)
    data.time = None #unknown
    data.name = 'solved by fwd_solve_1st_order'


    # b = b.transpose().resize(10,10)
    # v = np.linalg.solve(a.todense(),b.todense())
    from scipy.sparse.linalg import splu
    # x = np.linalg.solve(a.todense(),(b.todense()))
    # LU = splu(a)
    # x = LU.solve(b.todense())

    # v(idx,:)= left_divide(s_mat.E(idx, idx), pp.QQ(idx,:))
    return data
