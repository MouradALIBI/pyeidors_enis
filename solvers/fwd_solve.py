from deprecated.np_fwd_solve import np_fwd_solve
from models.mdl_normalize import mdl_normalize
from solvers.forward.fwd_solve_1st_orde import fwd_solve_1st_orde
from tools.eidors_default import eidors_default


class WS:
    def __init__(self, identifier, state):
        self.identifier = identifier
        self.state = state
class Copt:
    def __init__(self,fstr,cache_obj,boost_priority):
        self.fstr=fstr
        self.cache_obj=cache_obj
        self.boost_priority=boost_priority



def prepare_model(mdl):
    # mdl = mdl_normalize.set_flag(mdl, mdl_normalize.get_flag(mdl))
    mdl.normalize_measurements = 0
    mdl.n_elem = len(mdl.elems)
    mdl.n_node = len(mdl.nodes)
    mdl.n_elec = len(mdl.electrode)
    return mdl


def fwd_solve(fwd_model, img, data):
    """
     FWD_SOLVE: calculate data from a fwd_model object and an image

 fwd_solve can be called as
    data= fwd_solve( img)
 or (deprecated)
    data= fwd_solve( fwd_model, img)

 in each case it will call the fwd_model.solve
                        or img.fwd_model.solve method

 For reconstructions on dual meshes, the interpolation matrix
    is defined as fwd_model.coarse2fine. If required, this takes
    coarse2fine * x_coarse = x_fine

 data      is a measurement data structure
 fwd_model is a fwd_model structure
 img       is an img structure

 Options: (not available on all solvers)
    img.fwd_solve.get_all_meas = 1 (data.volt = all FEM nodes, but not CEM)
    img.fwd_solve.get_all_nodes= 1 (data.volt = all nodes, including CEM)
    img.fwd_solve.get_elec_curr= 1 (data.elec_curr = current on electrodes)

 Parameters:
    fwd_model.background = constant conductivity offset added to elem_data
    fwd_model.coarse2fine = linear mapping between img.elem_data and model parameters
    img.params_mapping = function mapping img.elem_data to model parameters
    """
    ws = WS("EIDORS:DeprecatedInterface", "on")
    # fwd_model = img.fwd_model               #  unitule
    fwd_model = prepare_model(fwd_model)
    solver = fwd_model.solve
    copt = Copt('fwd_solve',img,-2) # Boost-priority = -2  ==>>  fmdl evaluations are low priority
    if  solver == 'np_fwd_solve':
        data = np_fwd_solve(img)
    elif solver == "eidors_default":
        #data = eidors_default(img)
        data = fwd_solve_1st_orde(fwd_model, img,data)
        data.type='data'
    solver = fwd_model.solve
    solver = 'fwd_solve_1st_order'
    return data
