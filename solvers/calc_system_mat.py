from deprecated.np_calc_system_mat import np_calc_system_mat


class Copt:
    def __init__(self, cache_obj, fstr):
        self.cache_obj = cache_obj
        self.fstr = fstr


def calc_system_mat(fwd_model, img):
    """
     CALC_SYSTEM_MAT: calculate FEM system matrix from fwd_model and image

    system_mat= calc_system_mat( fwd_model, image)
 OR
    system_mat= calc_system_mat( image)

 it will call the fwd_model.system_mat

 if fwd_model.system_mat is a matrix, calc_system_mat will return this
 matrix

 system_mat
   system_mat.E    is FEM system_matrix
   system_mat.perm is permutation of E  i.e. E(perm,perm)
 fwd_model is a fwd_model structure
 image     is an image structure

system_mat= feval(fwd_model.system_mat, fwd_model, img);return

    """
    fwd_model = img.fwd_model
    cache_obj = [fwd_model, img.elem_data]
    copt = Copt(cache_obj, 'system_mat')
    system_mat = np_calc_system_mat(fwd_model, img)

    system_mat = 0
    return system_mat
